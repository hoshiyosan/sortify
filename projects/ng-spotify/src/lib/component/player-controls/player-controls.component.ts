import { Component, OnInit, NgZone } from '@angular/core';
import { StreamingService } from '../../streaming.service';
import { PlayerState } from '../../player.types';

@Component({
  selector: 'spotify-player-controls',
  templateUrl: './player-controls.component.html',
  styleUrls: ['./player-controls.component.css']
})
export class PlayerControlsComponent implements OnInit {
  state: PlayerState;

  constructor(
    private streaming: StreamingService
  ) { }

  ngOnInit() {
    this.streaming.state.subscribe(state=>{
      this.state = state;
    });
  }

  previousTrack(){
    this.streaming.playPreviousTrack();
  }

  togglePlayback(){
    this.streaming.togglePlayback();
  }

  nextTrack(){
    this.streaming.playNextTrack();
  }

}
