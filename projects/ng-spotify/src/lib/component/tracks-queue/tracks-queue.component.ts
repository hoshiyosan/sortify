import { Component, OnInit } from '@angular/core';
import { StreamingService } from '../../streaming.service';

@Component({
  selector: 'spotify-tracks-queue',
  templateUrl: './tracks-queue.component.html',
  styleUrls: ['./tracks-queue.component.css']
})
export class TracksQueueComponent implements OnInit {
  tracks_queue: any;

  constructor(private streaming: StreamingService) { }

  ngOnInit() {
    this.streaming.state.subscribe(state=>{
      this.tracks_queue = state.track_window;
    })
  }

  setPlayingTrack(track_id: string){
    this.streaming.setPlayingTrack(track_id);
  }

}
