import { Component, OnInit } from '@angular/core';
import { StreamingService } from '../../streaming.service';
import { Device } from '../../spotify.types';

@Component({
  selector: 'spotify-select-device',
  templateUrl: './select-device.component.html',
  styleUrls: ['./select-device.component.css']
})
export class SelectDeviceComponent implements OnInit {
  devices: Device[] = [];

  constructor(private streaming: StreamingService) { }

  ngOnInit() {
    this.streaming.devices.subscribe(devices=>{
      this.devices = devices;
    });
  }

  updateDevices(){
    this.streaming.updateDevices();
  }

  selectDevice(device_id: string){
    this.streaming.streamOnDevice(device_id)
    .subscribe();
  }

}
