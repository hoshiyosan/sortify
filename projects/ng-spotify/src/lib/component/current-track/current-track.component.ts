import { Component, OnInit } from '@angular/core';
import { PlayerState } from '../../player.types';
import { StreamingService } from '../../streaming.service';
import { MsToReadablePipe } from '../../pipe/ms-to-readable.pipe';

@Component({
  selector: 'spotify-current-track',
  templateUrl: './current-track.component.html',
  styleUrls: ['./current-track.component.css']
})
export class CurrentTrackComponent implements OnInit {
  state: PlayerState;

  constructor(private streaming: StreamingService) { }

  ngOnInit() {
    this.streaming.state.subscribe(state=>{
      this.state = state;
    });
  }

  onPositionChange(){
    this.streaming.seekCurrentlyPlayingProgress(this.state.position);
  }

}
