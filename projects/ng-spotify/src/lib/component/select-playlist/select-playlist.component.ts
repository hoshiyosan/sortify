import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../spotify.service';
import { StreamingService } from '../../streaming.service';

@Component({
  selector: 'spotify-select-playlist',
  templateUrl: './select-playlist.component.html',
  styleUrls: ['./select-playlist.component.css']
})
export class SelectPlaylistComponent implements OnInit {
  playlists: any[];

  constructor(
    private spotify: SpotifyService,
    private streaming: StreamingService
  ) { }

  ngOnInit() {}

  updatePlaylists(){
    this.spotify.getPlaylists()
    .subscribe(playlists=>{
      this.playlists = playlists;
    })
  }

  setPlayingPlaylist(playlist_id: string){
    this.streaming.setPlayingPlaylist(playlist_id);
  }

}
