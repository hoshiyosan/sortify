import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ms2readable'
})
export class MsToReadablePipe implements PipeTransform {

  transform(ms: number, ...args: any[]): any {
    const isostring = new Date(ms).toISOString().substr(11, 8);
    return (isostring.substr(0, 2)=='00') ? isostring.substr(3, 5) : isostring;
  }

}
