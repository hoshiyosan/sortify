import { Injectable } from '@angular/core';
import { SpotifyOauthService } from './spotify-oauth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PagingObject, Device } from './spotify.types';
import { forkJoin, Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';

const MAX_LIMIT = 50;

@Injectable({
  providedIn: 'root'
})
export class SpotifyService extends SpotifyOauthService{
  constructor(route: ActivatedRoute, http: HttpClient, router: Router){
    super(route, http, router);
  }

  getAllItems(uri: string){
    /*
    Retrieved a all items by chuncks
    */
    return this.get(uri + 'offset=0&limit=1')
    .pipe(
      flatMap((result: PagingObject)=>{

        let requests = [];
        for(let offset=0; offset<result.total; offset+=MAX_LIMIT){
          requests.push(
            this.get(uri + 'offset='+offset+'&limit='+MAX_LIMIT)
          );
        }
        return forkJoin(requests);
      }),
      map((results: PagingObject[])=>{
        let tracks = [];
        for(let result of results){
          tracks.push(...result.items);
        }
        return tracks;
      })
    )
  }

  getUserDetails(){
    return this.get('/v1/me');
  }

  getPlaylistTracks(playlist_id: string){
    return this.getAllItems('/v1/playlists/'+playlist_id+'/tracks?')
    .pipe(map(
      items=>{
        return items.map(item=>item.track)
      }
    ));
  }

  getLikedTracks(){
    return this.getAllItems('/v1/me/tracks?')
    .pipe(map(
      items=>{
        return items.map(item=>item.track)
      }
    ));
  }

  getPlaylists(){
    return this.getAllItems('/v1/me/playlists?');
  }

  getPlaylistsTracks(){
    return this.getPlaylists()
    .pipe(
      flatMap(playlists=>{
        let requests = [];
        for(let playlist of playlists){
          requests.push(
            this.getPlaylistTracks(playlist.id)
          );
        }
        return forkJoin(requests);
      }),
      map((playlist:any)=>{
        let all_tracks = [];

        for(let tracks of playlist){
          all_tracks.push(...tracks);
        }

        return all_tracks;
      })
    )
  }

  getAllTracks(){
    const requests = [
      this.getLikedTracks(),
      this.getPlaylistsTracks()
    ]
    return forkJoin(requests)
    .pipe(
      map(sources=>{
        let all_tracks = [];
        for(let tracks of sources){
          all_tracks.push(...tracks);
        }
        console.log(all_tracks.slice(0,3));
        return all_tracks;
      }
    ))
  }

  getAvailableDevices(): Observable<Device[]>{
    return this.get('/v1/me/player/devices')
    .pipe(
      map(resp=>{
        return resp['devices']
      })
    )
  }

  /* about playback */
  getCurrentlyPlaying(){
    return this.get('/v1/me/player/currently-playing');
  }

}