import { Injectable } from '@angular/core';
import { Grant, Oauth2Config } from './spotify-oauth.types';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as URL from 'url';

@Injectable({
  providedIn: 'root'
})
export class SpotifyOauthService{
  private api_url: string = "https://api.spotify.com/";
  private authenticated: boolean = false;
  private grant: Grant;

  constructor(
    private route: ActivatedRoute, 
    private http: HttpClient,
    private router: Router
  ) { }

  public getToken(){
    return this.grant.access_token;
  }

  public isAuthenticated(){
    return this.authenticated;
  }

  authorize(oauth: Oauth2Config){
    // redirect user to spotify auth page
    window.location.href = 
      'https://accounts.spotify.com/authorize?'
        + 'client_id=' + oauth.client_id
        + '&scope=' + encodeURIComponent(oauth.scopes.join(' '))
        + '&redirect_uri=' + oauth.redirect_uri
        + '&response_type=token';
  }

  loadGrantFromUrl(){
    const params = window.location.hash.substr(1)
      .split('&').reduce(function (result, item) {
          var parts = item.split('=');
          result[parts[0]] = parts[1];
          return result;
    }, {});

    // workout absolute expiration date
    let now = new Date();
    let remaining = parseInt(params['expires_in']) || 0;
    now.setSeconds(now.getSeconds() + remaining)
    
    this.setGrant({
      access_token: params['access_token'],
      token_type: params['token_type'],
      expires_at: now
    })

    this.router.navigateByUrl('/');
  }

  setGrant(grant: Grant){
    console.log('set grant');
    console.log(grant);

    this.grant = grant;
    this.authenticated = true;
    localStorage.setItem(
        'SPOTIFY_GRANT', JSON.stringify(this.grant));
    this.setGrantTimeout();
  }

  clearGrant(){
    console.log('clear grant');
    this.grant = null;
    this.authenticated = false;
    localStorage.removeItem('SPOTIFY_GRANT');
  }

  setGrantTimeout(){
    const now = new Date();
    const remaining = this.grant.expires_at.getTime() - now.getTime();
    console.log('grant expire in ' + Math.floor(remaining/1000) + ' s');

    setTimeout(()=>{
      this.clearGrant();
    }, remaining);
  }

  assertAuthenticated(){
    if(!this.isAuthenticated()){
      throw "AuthError: Can't query API without a valid Grant!";
    }
  }

  get(url) {
    this.assertAuthenticated();

    return this.http.get(
      URL.resolve(this.api_url, url), {
      headers: {
        'Authorization': this.grant.token_type + ' ' + this.grant.access_token
      }
    });
  }

  put(url:string, data: any){
    this.assertAuthenticated();

    return this.http.put(
      URL.resolve(this.api_url, url), data, {
      headers: {
        'Authorization': this.grant.token_type + ' ' + this.grant.access_token
      }
    });
  }

  post(url, data) {
    this.assertAuthenticated();

    return this.http.post(
      URL.resolve(this.api_url, url), data, {
      headers: {
        'Authorization': this.grant.token_type + ' ' + this.grant.access_token
      }
    });
  }

  loadGrantFromLocalStorage(){
    const grant = JSON.parse(
      localStorage.getItem('SPOTIFY_GRANT')
    );

    try{
      this.setGrant({
        access_token: grant['access_token'],
        token_type: grant['token_type'],
        expires_at: new Date(grant['expires_at'])
      });
    }catch{
      console.log('no token can be reload');
      this.clearGrant();
    }
    
  }
}
