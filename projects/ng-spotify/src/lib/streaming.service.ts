import { Injectable, NgZone } from '@angular/core';
import { SpotifyService } from './spotify.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Device } from './spotify.types';
import { PlayerState, HttpResponse } from './player.types';
declare var Spotify: any;



@Injectable({
  providedIn: 'root'
})
export class StreamingService {
  private _devices = new BehaviorSubject<Device[]>([]);
  public readonly devices: Observable<Device[]> = this._devices.asObservable();

  private _state = new BehaviorSubject<PlayerState>({
    position: 0, 
    duration: 0, 
    paused: true, 
    shuffle: false, 
    repeat_mode: 0
  })
  public readonly state: Observable<PlayerState> = this._state.asObservable();

  constructor(
    private spotify: SpotifyService,
    private ngzone: NgZone
  ) { 
    const tick = 300; // refresh interval in ms
    // increment position after 1 second if playing
    setInterval(()=>{
      const state:PlayerState = this._state.value;
      if(!state.paused){
        this._state.next({
          ...state,
          position: state.position + tick
        });
      }
    }, tick);
  }

  updateState(newState: PlayerState){
    this.ngzone.run(()=>{
      this._state.next(newState);
    })
  }

  loadSdk(){
    let script = document.createElement('script');
    script.src = 'https://sdk.scdn.co/spotify-player.js';
    script.type = 'text/javascript';
    script.async = true;
    document.head.appendChild(script);
  }

  loadPlayer(){
    const randint = (a: number, b: number) => Math.floor(Math.random()*(b-a) + a);
    const randid = () => randint(10**2, 10**3);

    window['onSpotifyWebPlaybackSDKReady'] = (data) => {
      let token: string;
      try{ token = this.spotify.getToken() }
      catch(error){ 
        // call authentication error handler and exit
        this.onAuthenticationError(); 
        throw "Cannot start player while not authenticated";
      }
      
      const player = new Spotify.Player({
        name: 'Sortify-'+randid(),
        getOAuthToken: cb => { cb(token); }
      });

      // Error handling
      player.addListener('initialization_error', ({ message }) => { console.error(message); });
      player.addListener('authentication_error', ({ message }) => { console.error(message); });
      player.addListener('account_error', ({ message }) => { console.error(message); });
      player.addListener('playback_error', ({ message }) => { console.error(message); });
    
      // Playback status updates
      player.addListener('player_state_changed', (newState: PlayerState) => {
        this.onPlayerStateChange(newState);
      });
    
      // Ready
      player.addListener('ready', data => this.onDeviceReady(data));
    
      // Not Ready
      player.addListener('not_ready', ({ device_id }) => {
        console.log('Device ID has gone offline', device_id);
      });
    
      // Connect to the player!
      player.connect();
    };

    this.loadSdk();
  }

  /* event handlers */
  onDeviceReady(data: any){
    console.log('device ready');
  }

  onAuthenticationError(){
    console.log('define custom handler');
  }

  onPlayerStateChange(newState: PlayerState){
    console.log('player state changed');
    console.log(newState);
    this.updateState(newState);
  }

  /* Custom exceptions handlers */
  onNoActiveDevice(){
    alert('no active player');
  }
  /* */
  /*updateCurrentTrack(){
    this.spotify.getCurrentlyPlaying()
    .subscribe(track=>{
      if(track){
        console.log('currently playing track');
        console.log(track);
      }else{
        console.log('no currently playing track');
        this.updateState({
          playing: false
        });
      }
    })
  }*/

  /* playback functions */
  updateDevices(){
    this.spotify.getAvailableDevices()
    .subscribe(
      (devices: Device[])=>{
        this._devices.next(devices);
      })
  }

  streamOnDevice(device_id: string){
    return this.spotify.put('/v1/me/player', {
      device_ids: [device_id]
    });
  }

  /* BEGIN pause, resume and toggle playback reading */

  pausePlayback(){
    this.spotify.put('/v1/me/player/pause', {})
    .subscribe(
      (resp: any)=>{}, 
      (resp: HttpResponse)=>{
        console.log('error while trying to pause playback');
        if(resp.error.error.reason == 'NO_ACTIVE_DEVICE'){
          this.onNoActiveDevice();
        }
      }
    );
  }

  resumePlayback(){
    return this.spotify.put('/v1/me/player/play', {})
    .subscribe(
      ()=>{}, 
      (resp: HttpResponse)=>{
        console.log('error while trying to pause playback');
        if(resp.error.error.reason == 'NO_ACTIVE_DEVICE'){
          this.onNoActiveDevice();
        }
      }
    );
  }

  togglePlayback(){
    if(this._state.value.paused){
      this.resumePlayback();
    }else{
      this.pausePlayback();
    }
  }
  /* END pause, resume and toggle playback reading */

  /* START skip to previous/next track */
  playPreviousTrack(){
    return this.spotify.post('/v1/me/player/previous', {})
    .subscribe();
  }

  playNextTrack(){
    return this.spotify.post('/v1/me/player/next', {})
    .subscribe();
  }
  /* END skip to previous/next track */

  setPlayingTrack(...tracks_id: string[]){
    return this.spotify.put('/v1/me/player/play', {
      uris: tracks_id.map(track_id=>"spotify:track:"+track_id)
    })
    .subscribe();
  }

  setPlayingPlaylist(playlist_id: string){
    return this.spotify.put('/v1/me/player/play', {
      context_uri: "spotify:playlist:"+playlist_id
    })
    .subscribe();
  }

  seekCurrentlyPlayingProgress(position_ms: number){
    this.spotify.put('/v1/me/player/seek?position_ms='+position_ms, {})
    .subscribe();
  }
}
