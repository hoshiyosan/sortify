import { NgModule } from '@angular/core';
import { CallbackComponent } from './callback/callback.component';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatSliderModule} from '@angular/material/slider';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';

import { MsToReadablePipe } from './pipe/ms-to-readable.pipe';
import { SpotifyService } from './spotify.service';
import { StreamingService } from './streaming.service';
import { PlayerControlsComponent } from './component/player-controls/player-controls.component';
import { CurrentTrackComponent } from './component/current-track/current-track.component';
import { SelectDeviceComponent } from './component/select-device/select-device.component';
import { TracksQueueComponent } from './component/tracks-queue/tracks-queue.component';
import { SelectPlaylistComponent } from './component/select-playlist/select-playlist.component';
import { GenreFilterComponent } from './filter-component/genre-filter/genre-filter.component';

const routes: Routes = [{
  path: 'spotify/authorized', component: CallbackComponent
}];

@NgModule({
  declarations: [
    CallbackComponent, 
    SelectDeviceComponent, 
    MsToReadablePipe, 
    PlayerControlsComponent, 
    CurrentTrackComponent, 
    TracksQueueComponent, 
    SelectPlaylistComponent, 
    GenreFilterComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    // material
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatSliderModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [],
  exports: [
    RouterModule, 
    SelectDeviceComponent, 
    PlayerControlsComponent, 
    CurrentTrackComponent, 
    TracksQueueComponent,
    SelectPlaylistComponent,
    GenreFilterComponent
  ]
})
export class SpotifyModule { 
  constructor(
    private spotify: SpotifyService,
    private streaming: StreamingService
  ){
    this.spotify.loadGrantFromLocalStorage();
    this.streaming.loadPlayer();
  }
}
