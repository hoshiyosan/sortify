export interface PagingObject{
    href: string;
    items: any[];
    limit: number;
    next: string;
    offset: number;
    previous: string;
    total: number;
}

export class Device {
    id: string;
    is_active: boolean;
    is_private_session: boolean;
    is_restricted: boolean;
    name: string;
    type: string;
}
