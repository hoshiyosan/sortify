import { Device } from './spotify.types';

export interface PlayerError{
    status: number,
    message: string,
    reason: string
}

export interface HttpResponse{
    error?: {error: PlayerError};
    headers: object;
    message: string;
    name: string;
    ok: boolean;
    status: number;
    statusText: string;
    url: string;
}

export interface PlayerState{
    position: number, 
    duration: number, 
    paused: boolean, 
    shuffle: boolean, 
    repeat_mode: number, 
    track_window?: any
}