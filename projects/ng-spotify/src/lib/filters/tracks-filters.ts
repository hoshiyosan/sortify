
// @dynamic
export class TracksFilter{
    static unique(): Function{
        return (tracks: any[]) => {
            let result = [];
            let map = new Map();
            for(let item of tracks){
                if(!map.has(item.id)){
                map.set(item.id, true);
                result.push(item);
                }
            }
            return result;
        }
    }

    static releasedBetween(start_date: string, end_date: string){
        return (tracks: any[]) => {
            return tracks.filter(track=>
                track.album.release_date >= start_date
                && track.album.release_date <= end_date )
        }
    }

    static filter(tracks: any[], ...filters: Function[]){
        let filteredTracks = tracks;
        for(let filter of filters){
            filteredTracks = filter(filteredTracks);
        }
        return filteredTracks;
    }
}