import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../spotify.service';

@Component({
  selector: 'lib-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor(private spotify: SpotifyService) { }

  ngOnInit() {
    console.log('callback');
    this.spotify.loadGrantFromUrl();
  }

}
