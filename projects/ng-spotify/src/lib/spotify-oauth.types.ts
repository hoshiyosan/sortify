export enum SpotifyScope{
    PLAYLIST_READ_COLLABORATIVE = 'playlist-read-collaborative',
    PLAYLIST_MODIFY_PRIVATE = 'playlist-modify-private',
    PLAYLIST_MODIFY_PUBLIC = 'playlist-modify-public',
    PLAYLIST_READ_PRIVATE = 'playlist-read-private',
    USER_MODIFY_PLAYBACK_STATE = 'user-modify-playback-state',
    USER_READ_CURRENTLY_PLAYING = 'user-read-currently-playing',
    USER_READ_PLAYBACK_STATE = 'user-read-playback-state',
    USER_READ_PRIVATE = 'user-read-private',
    USER_READ_EMAIL = 'user-read-email',
    USER_LIBRARY_MODIFY = 'user-library-modify',
    USER_LIBRARY_READ = 'user-library-read',
    USER_FOLLOW_MODIFY = 'user-follow-modify',
    USER_FOLLOW_READ = 'user-follow-read',
    USER_READ_RECENTLY_PLAYED = 'user-read-recently-played',
    USER_TOP_READ = 'user-top-read',
    STREAMING = 'streaming',
    WEB_PLAYBACK_SDK = 'web-playback-sdk'
}

export interface Oauth2Config{
    client_id: string,
    redirect_uri: string,
    scopes: SpotifyScope[]
}

export interface Grant{
    access_token: string;
    token_type: string;
    expires_at: Date;
}
