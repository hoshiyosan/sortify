import { Component } from '@angular/core';
import { SpotifyService } from "ng-spotify";
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sortify';

  constructor( private spotify: SpotifyService ){}

  isAuthenticated(){
    return this.spotify.isAuthenticated();
  }
}
