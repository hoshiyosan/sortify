import { Component, OnInit, Input } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-tracks-table',
  templateUrl: './tracks-table.component.html',
  styleUrls: ['./tracks-table.component.scss']
})
export class TracksTableComponent implements OnInit {
  displayedColumns: string[] = ['tracks'];
  _tracks: any[] = [];
  dataSource = new MatTableDataSource(this._tracks);

  @Input()
  set tracks(tracks: any[]){
    this._tracks = tracks;
    this.dataSource = new MatTableDataSource(this._tracks);
  }
  
  constructor() { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
