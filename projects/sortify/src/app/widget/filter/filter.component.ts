import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Filter } from '../../model/filter/filter';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  _filters: Filter[] = [];
  selected_filter: string;
  filter_types: string[] = [ "date", "genre", "beat" ];
  btn_add_disabled: boolean = true;

  /* filter io */
  @Output() filtersChange: EventEmitter<object[]> = new EventEmitter<object[]>();
  @Input() set filters(filters: Filter[]) {
    this._filters = filters;
  }

  constructor() { }

  ngOnInit() {
  }

  filtersChanged(){
    this.filtersChange.emit(this._filters);
  }

  addFilter(){
    this._filters.push({
      type: this.selected_filter
    });
    console.log(this._filters);
    this.selected_filter = null;
    console.log(this._filters);
  }

  deleteFilter(index: number){
    this._filters.splice(index, 1);
    console.log(this.filters);
  }

  onSubmit(){
    console.log(this.filters);
  }

}
