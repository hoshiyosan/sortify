import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeatFilterComponent } from './beat-filter.component';

describe('BeatFilterComponent', () => {
  let component: BeatFilterComponent;
  let fixture: ComponentFixture<BeatFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeatFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeatFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
