import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DateFilter } from '../../../model/filter/date-filter';

@Component({
  selector: 'app-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss']
})
export class DateFilterComponent implements OnInit {
  _filter: DateFilter;

  @Output() filterChange: EventEmitter<DateFilter> = new EventEmitter<DateFilter>();
  @Input() set filter(_filter: DateFilter) {
    this._filter = _filter
  }

  constructor() { }

  ngOnInit() {}

  filterChanged(){
    this.filterChange.emit(this._filter);
  }

}
