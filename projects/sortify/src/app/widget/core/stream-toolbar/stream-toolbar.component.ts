import { Component, OnInit } from '@angular/core';
import { StreamingService } from 'ng-spotify';

@Component({
  selector: 'app-stream-toolbar',
  templateUrl: './stream-toolbar.component.html',
  styleUrls: ['./stream-toolbar.component.scss']
})
export class StreamToolbarComponent implements OnInit {

  constructor(public player: StreamingService) { }

  ngOnInit() {
    /*
    this.player.state.playing.subscribe((playing:boolean)=>{
      console.log('state change ' + playing);
      if(playing){
        console.log('playing');
        this.playing = true;
      }else{
        console.log('not playing')
        this.playing = false;
      }
      //this.playing = Boolean(playing);
    });
    */
  }

}
