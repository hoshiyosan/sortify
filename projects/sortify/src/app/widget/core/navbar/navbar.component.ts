import { Component, OnInit } from '@angular/core';
import { SpotifyService, SpotifyScope } from 'ng-spotify';
import { Router } from '@angular/router';
import { environment } from 'projects/sortify/src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(public spotify: SpotifyService, private router: Router) { }

  ngOnInit() {}

  connectSpotify(){
    this.spotify.authorize({
      client_id: environment.spotify_client_id,
      redirect_uri: environment.spotify_redirect_uri,
      scopes:[
        SpotifyScope.USER_LIBRARY_READ,
        
        SpotifyScope.USER_READ_EMAIL,
        SpotifyScope.USER_READ_PRIVATE,
        SpotifyScope.USER_READ_PLAYBACK_STATE,

        SpotifyScope.STREAMING
      ]
    });
  }

  disconnectSpotify(){
    this.spotify.clearGrant();
    this.router.navigateByUrl('/');
  }

  sortPlaylists(){
    alert('sort playlists');
  }
}
