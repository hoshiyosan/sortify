import { Component, OnInit, Input } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-playlist-table',
  templateUrl: './playlist-table.component.html',
  styleUrls: ['./playlist-table.component.scss']
})
export class PlaylistTableComponent implements OnInit {
  displayedColumns: string[] = ['playlist', 'tracks'];
  dataSource = new MatTableDataSource([]);

  @Input()
  set playlists(playlists: any[]){
    this.dataSource = new MatTableDataSource(playlists);
  }
  
  constructor() { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
