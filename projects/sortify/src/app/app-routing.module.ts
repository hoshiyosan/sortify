import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './view/home/home.component';
import { ProfileComponent } from './view/profile/profile.component';
import { ProfileHomeComponent } from './view/profile/profile-home/profile-home.component';
import { SpotifyPlaylistsComponent } from './view/profile/spotify-playlists/spotify-playlists.component';
import { SpotifyTracksComponent } from './view/profile/spotify-tracks/spotify-tracks.component';
import { ProfileDetailsComponent } from './view/profile/profile-details/profile-details.component';
import { SpotifyAuthGuard } from './guard/spotify-auth.guard';
import { SortedPlaylistsComponent } from './view/sorted/sorted-playlists/sorted-playlists.component';
import { SortedPlaylistComponent } from './view/sorted/sorted-playlist/sorted-playlist.component';


const routes: Routes = [{
  path: '', component: HomeComponent,
}, {
  path: 'me', component: ProfileComponent,
  canActivate: [SpotifyAuthGuard],
  children: [{
    path: '',
    component: ProfileHomeComponent
  }, {
    path: 'playlists',
    component: SpotifyPlaylistsComponent
  }, {
    path: 'tracks',
    component: SpotifyTracksComponent
  }, {
    path: 'details',
    component: ProfileDetailsComponent
  }]
}, {
  path: 'sorted/playlists',
  component: SortedPlaylistsComponent
}, {
  path: 'sorted/playlist/:id',
  component: SortedPlaylistComponent
}, {
  path: 'sorted/playlist/create',
  component: SortedPlaylistComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
