import { Component, OnInit } from '@angular/core';
import { SortifyService } from '../../../service/sortify.service';
import { SortedPlaylist } from '../../../model/sorted-playlist';

@Component({
  selector: 'app-sorted-playlists',
  templateUrl: './sorted-playlists.component.html',
  styleUrls: ['./sorted-playlists.component.scss']
})
export class SortedPlaylistsComponent implements OnInit {
  playlists: SortedPlaylist[];

  constructor(private sortify: SortifyService) { }

  ngOnInit() {
    this.playlists = this.sortify.getSortedPlaylists();
  }

}
