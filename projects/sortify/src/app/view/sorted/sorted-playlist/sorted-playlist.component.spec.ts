import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortedPlaylistComponent } from './sorted-playlist.component';

describe('SortedPlaylistComponent', () => {
  let component: SortedPlaylistComponent;
  let fixture: ComponentFixture<SortedPlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortedPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortedPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
