import { Component, OnInit } from '@angular/core';
import { SortifyService } from '../../../service/sortify.service';
import { SortedPlaylist } from '../../../model/sorted-playlist';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sorted-playlist',
  templateUrl: './sorted-playlist.component.html',
  styleUrls: ['./sorted-playlist.component.scss']
})
export class SortedPlaylistComponent implements OnInit {
  action: string;
  playlist: SortedPlaylist;

  constructor(
    private sortify: SortifyService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      const id = params['id'];
      this.action = (id==='create') ? 'create' : 'update';

      if(id === 'create'){
        this.playlist = {
          title: '',
          public: false,
          filters: []
        }
      }else{
        this.playlist = this.sortify.getSortedPlaylist(id);
      }
    })
  }

  createPlaylist(){
    this.sortify.createSortedPlaylist(this.playlist);
    this.backToPlaylists();
  }

  backToPlaylists(){
    this.router.navigateByUrl('/sorted/playlists');
  }

  updatePlaylist(){
    this.sortify.updateSortedPlaylist(this.playlist);
    this.backToPlaylists();
  }

  dropSortedPlaylist(){
    this.sortify.dropSortedPlaylist(this.playlist);
    this.backToPlaylists();
  }

}
