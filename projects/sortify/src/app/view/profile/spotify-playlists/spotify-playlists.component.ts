import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'ng-spotify';

@Component({
  selector: 'app-spotify-playlists',
  templateUrl: './spotify-playlists.component.html',
  styleUrls: ['./spotify-playlists.component.scss']
})
export class SpotifyPlaylistsComponent implements OnInit {
  playlists: any[];

  constructor(private spotify: SpotifyService) { }

  ngOnInit() {
    this.spotify.getPlaylists()
    .subscribe(playlists=>{
      this.playlists = playlists;
    })
  }
}
