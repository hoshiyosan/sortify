import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'ng-spotify';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {
  user: any;

  constructor(private spotify: SpotifyService) { }

  ngOnInit() {
    this.spotify.getUserDetails()
    .subscribe(user=>{
      this.user = user;
    })
  }

}
