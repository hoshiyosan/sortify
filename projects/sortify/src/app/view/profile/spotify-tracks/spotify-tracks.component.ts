import { Component, OnInit } from '@angular/core';
import { SpotifyService, TracksFilter, StreamingService } from 'ng-spotify';

@Component({
  selector: 'app-spotify-tracks',
  templateUrl: './spotify-tracks.component.html',
  styleUrls: ['./spotify-tracks.component.scss']
})
export class SpotifyTracksComponent implements OnInit {
  tracks: any[];
  
  constructor(private spotify: SpotifyService, private streaming: StreamingService) { }

  ngOnInit() {
    this.spotify.getAllTracks()
    .subscribe(tracks=>{
      this.tracks = TracksFilter.filter(
        tracks, 
        TracksFilter.unique(),
        TracksFilter.releasedBetween('2000', '2005')
      );
    })
  }

  playAll(){
    this.streaming.setPlayingTrack(...this.tracks.map(track=>track.id));
  }

}
