import { Filter } from './filter';

export interface DateFilter extends Filter{
    type: 'date',
    startdate?: Date,
    enddate?: Date
}
