import { Filter } from './filter/filter';

export interface SortedPlaylist {
    id?: string;
    title: string;
    description?: string;

    public: boolean;
    filters: Filter[];
}
