import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// angular material
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatSliderModule} from '@angular/material/slider';
import {Ng5SliderModule} from 'ng5-slider';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './view/home/home.component';
import { SpotifyModule } from 'ng-spotify';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './view/profile/profile.component';

import { NavbarComponent } from './widget/core/navbar/navbar.component';
import { PlaylistTableComponent } from './widget/playlist/playlist-table/playlist-table.component';
import { TracksTableComponent } from './widget/tracks/tracks-table/tracks-table.component';
import { SpotifyPlaylistsComponent } from './view/profile/spotify-playlists/spotify-playlists.component';
import { SpotifyTracksComponent } from './view/profile/spotify-tracks/spotify-tracks.component';
import { ProfileHomeComponent } from './view/profile/profile-home/profile-home.component';
import { ProfileDetailsComponent } from './view/profile/profile-details/profile-details.component';
import { SortedPlaylistsComponent } from './view/sorted/sorted-playlists/sorted-playlists.component';
import { SortedPlaylistComponent } from './view/sorted/sorted-playlist/sorted-playlist.component';
import { ShareBarComponent } from './widget/core/share-bar/share-bar.component';
import { FilterComponent } from './widget/filter/filter.component';
import { DateFilterComponent } from './widget/filter/date-filter/date-filter.component';
import { GenreFilterComponent } from './widget/filter/genre-filter/genre-filter.component';
import { BeatFilterComponent } from './widget/filter/beat-filter/beat-filter.component';
import { StreamToolbarComponent } from './widget/core/stream-toolbar/stream-toolbar.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ProfileComponent,
    PlaylistTableComponent,
    TracksTableComponent,
    SpotifyPlaylistsComponent,
    SpotifyTracksComponent,
    ProfileHomeComponent,
    ProfileDetailsComponent,
    SortedPlaylistsComponent,
    SortedPlaylistComponent,
    ShareBarComponent,
    FilterComponent,
    DateFilterComponent,
    GenreFilterComponent,
    BeatFilterComponent,
    StreamToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    // custom modules
    SpotifyModule,

    // material
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatListModule,
    MatChipsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCardModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatMenuModule,
    MatSliderModule,
    Ng5SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
