import { Injectable } from '@angular/core';
import { SortedPlaylist } from '../model/sorted-playlist';
import { StorageService } from './storage.service';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class SortifyService {

  constructor(private storage: StorageService) { }

  public createSortedPlaylist(playlist: SortedPlaylist){
    let playlists = this.storage.get('sortedPlaylists');
    playlists = (!!playlists) ? playlists : [];

    playlist.id = uuid.v4();
    playlists.push(playlist);
    this.storage.set('sortedPlaylists', playlists);
  }

  public getSortedPlaylists(): SortedPlaylist[]{
    let playlists: SortedPlaylist[] = this.storage.get('sortedPlaylists');
    playlists = (!!playlists) ? playlists : [];
    return playlists;
  }

  public getSortedPlaylist(id: string): SortedPlaylist{
    let playlists: SortedPlaylist[] = this.getSortedPlaylists();
    return playlists.filter(playlist=>playlist.id == id)[0];
  }

  public updateSortedPlaylist(playlist: SortedPlaylist){
    console.log(playlist.filters);
    let playlists = this.getSortedPlaylists();
    let index = playlists.indexOf(this.getSortedPlaylist(playlist.id));
    playlists.splice(index, 1);
    playlists.splice(index, 0, playlist);
    this.storage.set('sortedPlaylists', playlists);
  }

  public dropSortedPlaylist(playlist: SortedPlaylist){
    let playlists = this.getSortedPlaylists();
    let index = playlists.indexOf(this.getSortedPlaylist(playlist.id));
    playlists.splice(index, 1);
    this.storage.set('sortedPlaylists', playlists);
  }
}
