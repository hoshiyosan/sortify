import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  set(key: string, value: any){
    localStorage.setItem(key, JSON.stringify(value));
  }

  get(key: string): any{
    const obj = JSON.parse(localStorage.getItem(key));
    console.log(obj);
    return obj;
  }

  del(key: string){
    localStorage.removeItem(key);
  }

}
