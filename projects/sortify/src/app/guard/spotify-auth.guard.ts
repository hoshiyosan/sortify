import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SpotifyService } from 'ng-spotify';

@Injectable({
  providedIn: 'root'
})
export class SpotifyAuthGuard implements CanActivate {
  constructor(private spotify: SpotifyService, private router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    if(!this.spotify.isAuthenticated()){
      this.router.navigateByUrl('/');
    }

    return true;
  }
  
}
