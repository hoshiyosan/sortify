help: ## display this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

links: ## create link to use ng-spotify in debug mode without reinstall
	cd dist/ng-spotify && npm link && cd ../..
	npm link ng-spotify

debug-spotify: ## build ng-spotify in debug mode
	ng build ng-spotify --watch

run-sortify: ## run sortify client in debug mode
	ng serve sortify

publish-spotify: ## clean build and publish ng-spotify package
	rm -rf dist/ng-spotify
	ng build ng-spotify
	cd dist/ng-spotify && npm publish

build-sortify: ## build sortify application for distribution
	ng build sortify --prod --base-href /

build-all:
	- npm run build:ng-spotify
	- npm run build:sortify
